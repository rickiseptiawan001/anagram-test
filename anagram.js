
// associative array
const array = ['kita', 'atik', 'tika', 'aku', 'makan', 'kia', 'kua'];

// variable
let hasil = [];
let objectTemporary = {};
let objectNow = {};

// looping associative array
for (let i = 0 ; i < array.length; i++) {
  let hasilTemporary = [];
  hasilTemporary.push(array[i]);
  for (let j = i+1; j < array.length; j++) {
    if (objectSame(objectFunc(array[i]), objectFunc(array[j]))) {
      hasilTemporary.push(array[j]);
      array.splice(j, 1);
      j--;
    } 
  }
  hasil.push(hasilTemporary);
}

// tampilkan
console.log(hasil)  

// fungsi mengecek apakah object key nya sama atau tidak
function objectSame(objectTemporary, objectNow){
  let keysObjectTemporary = Object.keys(objectTemporary);
  
  for(let i = 0;i<keysObjectTemporary.length;i++){
    if(objectTemporary[keysObjectTemporary[i]] != objectNow[keysObjectTemporary[i]]){
      return false;
    }
  }
  return true;
}

// fungsi pengulangan kata
function objectFunc(kata){
  let objResult = {};
  if(kata){
    for(let j = 0; j < kata.length; j++){
      if(objResult[kata[j]]){
        objResult[kata[j]] = objResult[kata[j]] + 1
      }
      else{
        objResult[kata[j]] = 1
      }
    }
  }
  return objResult;
}
